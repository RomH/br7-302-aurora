package magshimim.aurora;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.usage.UsageEvents;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.Vibrator;
import android.provider.AlarmClock;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.content.Context;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;



public class talkScreen extends Activity {
    SpeechRecognizer speechRecognizer;
    TextToSpeech textToSpeech;
    ImageButton btn;

    // The ML model protocol values
    AuroraConstValues constValues = new AuroraConstValues();

    // The area where the results are shown
    TextView txtV;
    boolean flashlightStatus = false;
    String loc = "";

    private FusedLocationProviderClient client;

    // Get messages from the server
    BufferedReader input;

    // The socket between the client and the server
    private Socket socket;

    // True when the server is free
    private boolean isTheServerFree = false;

    // True if failed to connect to the server
    private boolean connectionError = false;

    /************************************************************************
     *                           LOCATION VARS                              *
     ************************************************************************/

    private FusedLocationProviderClient fusedLocationClient;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;

    private LocationAddressResultReceiver addressResultReceiver;

    private Location currentLocation;

    private LocationCallback locationCallback;

    String location = "";

    String currentCity = "";
    String currentCountry = "";

    /************************************************************************
     *                             FUNCTIONS                                *
     ************************************************************************/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Go fullscreen.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Link to xml.
        setContentView(R.layout.talk_screen);

        getLocationInBackground locationInBackground = new getLocationInBackground();

        try{
            locationInBackground.execute();
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

        configureBackButton();

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = textToSpeech.setLanguage(Locale.UK);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(talkScreen.this,
                                "The language you used is not supported!",
                                Toast.LENGTH_SHORT);
                    } else {
                        textToSpeech.setPitch(1.2f);
                        textToSpeech.setSpeechRate(1.1f);
                    }
                }
            }
        });

        txtV = findViewById(R.id.textView2);
        btn = findViewById(R.id.auroraBtn);

        txtV.setCompoundDrawables(null, null, null, null);

        // Enable scrolling in the textView
        txtV.setMovementMethod(new ScrollingMovementMethod());

        /**************************************************************
         *                 LOCATION FUNCTIONS                         *
         **************************************************************/

        addressResultReceiver = new LocationAddressResultReceiver(new Handler());

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLocation = locationResult.getLocations().get(0);
                getAddress();
            }
        };
        startLocationUpdates();

        initializeSpeechRecognizer();
        listen();
    }

    /**
     * What happens when the back key is pressed?
     */
    @Override
    public void onBackPressed() {
        Disconnect disconnectThread = new Disconnect();
        disconnectThread.start();
        startActivity(new Intent(talkScreen.this, MainScreen.class));
        finish();
    }

    @Override
    protected void onDestroy(){
        Disconnect disconnectThread = new Disconnect();
        disconnectThread.start();
        super.onDestroy();
    }

    private class getLocationInBackground extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            getLocation();

            return null;
        }
    }

    /**
     * Use this function to make the app speak.
     * @param txt What to say?
     */
    private void speak(String txt) {
        textToSpeech.speak(txt, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    private void initializeSpeechRecognizer() {
        if (SpeechRecognizer.isRecognitionAvailable(this)) {
            speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);

            speechRecognizer.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle bundle) {
                    btn.setColorFilter(Color.parseColor("#90ee02"));
                }

                @Override
                public void onBeginningOfSpeech() {
                }

                @Override
                public void onRmsChanged(float v) {
                }

                @Override
                public void onBufferReceived(byte[] bytes) {
                }

                @Override
                public void onEndOfSpeech() {
                    btn.setColorFilter(Color.TRANSPARENT);
                }

                @Override
                public void onError(int i) {

                    btn.setColorFilter(Color.parseColor("#B71C1C"));
                    speechRecognizer.stopListening();
                }

                @Override
                public void onResults(Bundle bundle) {
                    List<String> results = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                    if (results != null) {
                        getConversation(results.get(0));
                    }
                }

                @Override
                public void onPartialResults(Bundle bundle) {
                }

                @Override
                public void onEvent(int i, Bundle bundle) {
                }
            });
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void getConversation(String query) {
        txtV.setText("");
        txtV.append(query + "\n");
        final AIConfiguration config = new AIConfiguration("a3b5e32a26114d90b20d708122ebd2c7",
                AIConfiguration.SupportedLanguages.English, AIConfiguration.RecognitionEngine.System);

        final AIDataService aiDataService = new AIDataService(this, config);

        final AIRequest aiRequest = new AIRequest();
        aiRequest.setQuery(query);

        new AsyncTask<AIRequest, Void, AIResponse>() {
            @Override
            protected AIResponse doInBackground(AIRequest... requests) {
                final AIRequest request = requests[0];
                try {
                    final AIResponse response = aiDataService.request(aiRequest);
                    return response;
                } catch (AIServiceException e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(AIResponse aiResponse) {
                if (aiResponse != null) {
                    processResults(aiResponse);
                }
            }
        }.execute(aiRequest);
    }

    private void processResults(AIResponse response) {
        final Result result = response.getResult();
        final String speech = result.getFulfillment().getSpeech();
        final String query = result.getResolvedQuery().toLowerCase();
        HashMap parameters = result.getParameters();

        if(parameters == null) //if the parameters are null due to a query such as "you're smart", initiate the object to avoid a null exception
        {
            parameters = new HashMap(1);
            parameters.put(constValues.PARAMETERS_DONT_EXIST, constValues.PARAMETERS_DONT_EXIST);
        }

        if (parameters.containsKey(constValues.CALL_VALUE)) { //call a contact
            speak(speech);
            txtV.append(speech);
            if(parameters.containsKey("given-name"))
            {
                call(parameters.get("given-name").toString());
            }
            else if (parameters.containsKey("Family"))
            {
                call(parameters.get("Family").toString());
            }
        }
        else if (parameters.containsKey(constValues.OPEN_VALUE)) //opening an app/process
        {
            txtV.append("opening " + query.substring("open ".length()));
            openProcess(query.substring("open ".length()));

        }
        else if (parameters.containsKey(constValues.TURN_ON_VALUE)) //turning on a resource (i.e wifi, bluetooth...)
        {
            speak(speech);
            txtV.append(speech);
            turnOnRecourse(result.getStringParameter("Resources").toLowerCase());
        }
        else if (parameters.containsKey(constValues.TURN_OFF_VALUE)) //turning off a resource (i.e wifi, bluetooth...)
        {
            speak(speech);
            txtV.append(speech);
            turnOffResource(result.getStringParameter("Resources").toLowerCase());
        }
        else if (query.contains("echo")) //repeating what the user said
        {
            speak(result.getResolvedQuery().toLowerCase().substring("echo ".length()));
            txtV.append(result.getResolvedQuery().toLowerCase().substring("echo ".length()));

        }
        else if(parameters.containsKey(constValues.LOCATION_VALUE))
        {
            if(loc == "error")
            {
                speak("couldn't find your location");
                return;
            }

            speak(speech + " " + loc);
            txtV.append(speech + " " + loc);
        }
        else if (parameters.containsKey(constValues.NEWS_VALUE)){
            getNews(parameters);
            speak(speech);
        }
        else if (query.contains("search ")){
            searchGoogle(query);
        }
        else if (parameters.containsKey(constValues.WEATHER_VALUE)){
            getWeather(parameters);
        }
        else if(parameters.containsKey(constValues.LOG_NAME_VALUE))
        {
            Preferences.setName(parameters.get("given-name").toString(), getApplicationContext());
            txtV.append(speech);
            speak(speech);
        }
        else if(parameters.containsKey(constValues.GET_NAME_VALUE))
        {
            speak(speech + Preferences.getName(getApplicationContext()));
            txtV.append(speech + " " + Preferences.getName(getApplicationContext()));
        }
        else if(parameters.containsKey(constValues.LOG_HEIGHT_VALUE))
        {
            String amount = " " + parameters.get("unit-length").toString().split(",")[0].split(":")[1];
            String unit = parameters.get("unit-length").toString().split(",")[1].split(":")[1].replaceAll("^\"|\"$", "");
            Preferences.setHeight(amount + " " + unit, getApplicationContext());
            txtV.append(speech);
            speak(speech);
        }
        else if(parameters.containsKey(constValues.GET_HEIGHT_VALUE))
        {
            speak(speech + Preferences.getHeight(getApplicationContext()));
            txtV.append(speech + Preferences.getHeight(getApplicationContext()));
        }
        else if(parameters.containsKey(constValues.LOG_WEIGHT_VALUE))
        {
            String amount = " " + parameters.get("unit-weight").toString().split(",")[0].split(":")[1];
            String unit = parameters.get("unit-weight").toString().split(",")[1].split(":")[1].replaceAll("^\"|\"$", "");
            Preferences.setWeight(amount + " " + unit, getApplicationContext());
            txtV.append(speech);
            speak(speech);
        }
        else if(parameters.containsKey(constValues.GET_WEIGHT_VALUE))
        {
            speak(speech + Preferences.getWeight(getApplicationContext()));
            txtV.append(speech + Preferences.getWeight(getApplicationContext()));
        }
        else if (parameters.containsKey(constValues.ALARM))
        {
            speak(speech);

            int hour = Integer.parseInt(parameters.get("time").toString().split(":")[0].replaceAll("^\"|\"$", ""));
            if(hour > 24 || hour < 0)
            {
                Toast.makeText(getApplicationContext(), "Please enter a valid hour", Toast.LENGTH_SHORT).show();
                return;
            }

            int minutes = Integer.parseInt(parameters.get("time").toString().split(":")[1].replaceAll("^\"|\"$", ""));
            if(minutes > 59 || minutes < 0)
            {
                Toast.makeText(getApplicationContext(), "Please enter a valid minute", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
            intent.putExtra(AlarmClock.EXTRA_HOUR, hour);
            intent.putExtra(AlarmClock.EXTRA_MINUTES, minutes);
            startActivity(intent);
        }
        else if(parameters.containsKey(constValues.ACTIVATE_MIC))
        {
            speak(speech);
            try {
                    Thread.sleep(2000);
            } catch (Exception e)
            {

            }
            listenFnc();
        }
        else if(parameters.containsKey(constValues.SING))
        {
            Songs song = new Songs();
            HashMap<String, String> chosenSong = song.pickRandom();
            speak("Playing " + chosenSong.keySet().toArray()[0]);
            txtV.append("Playing " + chosenSong.keySet().toArray()[0] + "\n");
            speak(chosenSong.get(chosenSong.keySet().toArray()[0]));
            txtV.append(chosenSong.get(chosenSong.keySet().toArray()[0]));
        }
        else if (parameters.containsKey(constValues.FLIP_A_COIN))
        {
            String headsTails = "";
            final int min = 1;
            final int max = 2;
            final int random = new Random().nextInt((max - min) + 1) + min;

            if(random == 1)
            {
                headsTails = "heads";
            }
            else if (random == 2)
            {
                headsTails = "tails";
            }

            speak(speech + " " + headsTails + "!");
            txtV.append(speech + " " + headsTails + "!");
        }
        else if (parameters.containsKey(constValues.GENERATE_RANDOM))
        {
            String numbers = parameters.get("number").toString();

            int random = 0;

            if(numbers.equals("[]"))
            {
                final int min = 0;
                final int max = 100;
                random = new Random().nextInt((max - min) + 1) + min;
            }
            else if (!numbers.contains(","))
            {
                if(query.contains("up") || query.contains("less"))
                {
                    try {
                        final int min = 0;
                        final int max = Integer.parseInt(numbers.split("\\.")[0].substring(numbers.indexOf("[") + 1));
                        random = new Random().nextInt((max - min) + 1) + min;
                    } catch (Exception exception)
                    {
                        speak("Couldn't generate the number! Please try again");
                        txtV.append("Couldn't generate the number! Please try again");
                        return;
                    }
                }
                else if(query.contains("above") || query.contains("more"))
                {
                    try {
                        final int min = Integer.parseInt(numbers.split("\\.")[0].substring(numbers.indexOf("[") + 1));
                        final int max = min + 1000;
                        random = new Random().nextInt((max - min) + 1) + min;
                    } catch (Exception exception)
                    {
                        speak("Couldn't generate the number! Please try again");
                        txtV.append("Couldn't generate the number! Please try again");
                        return;
                    }

                }
            }
            else if (numbers.contains(","))
            {
                try {
                    final int min = Integer.parseInt(numbers.split(",")[0].split("\\.")[0].substring(numbers.indexOf("[") + 1));
                    final int max = Integer.parseInt(numbers.split(",")[1].split("\\.")[0]);
                    random = new Random().nextInt((max - min) + 1) + min;
                } catch (Exception exception)
                {
                    speak("Couldn't generate the number! Please try again");
                    txtV.append("Couldn't generate the number! Please try again");
                    return;
                }
            }

            speak(speech + " " + random);
            txtV.append(speech + " " + random);
        }
        else if (parameters.containsKey(constValues.WHAT_CAN_YOU_DO))
        {
            speak("Hello " + Preferences.getName(getApplicationContext()) + " I'm Aurora, your personal assistant! " +
                    "I can do many things! Try asking me: 'What's the weather like in Tel Aviv' or just have a small talk with me! " +
                    "I can generate a random number for you, flip a coin, sing a popular song, set an alarm, log your height or weight, call your mom or turn on WiFi! " +
                    "Go ahead! Try me out!");
            txtV.append("Hello " + Preferences.getName(getApplicationContext()) + " I'm Aurora, your personal assistant!\n" +
                    "I can do many things! Try asking me: 'What's the weather like in Tel Aviv' or just have a small talk with me! \n" +
                    "I can generate a random number for you, flip a coin, sing a popular song, set an alarm, log your height or weight, call your mom or turn on WiFi! \n" +
                    "Go ahead! Try me out!");
        }
        else if (parameters.containsKey(constValues.REMEMBER_VALUE))
        {
            String previous = Preferences.getRemember(getApplicationContext());
            String toAppend = previous + "\n" + query;
            Preferences.setRemember(toAppend, getApplicationContext());
            txtV.append(speech);
            speak(speech);
        }
        else if(parameters.containsKey(constValues.GET_REMEMBER_VALUE))
        {
            if(Preferences.getRemember(getApplicationContext()) == "")
            {
                speak("You have told me nothing yet!");
                txtV.append("You have told me nothing yet!");
            }
            else
            {
                txtV.append(speech + Preferences.getRemember(getApplicationContext()));
                speak(speech + " " + Preferences.getRemember(getApplicationContext()));
            }
        }
        else if(parameters.containsKey(constValues.RESET_REMEMBER))
        {
            Preferences.setRemember("", getApplicationContext());
            speak(speech);
        }
        else
        {
            speak(speech);
            txtV.append(speech);
        }
    }

    void getWeather(HashMap parameters){
        String messageCode = "10";
        String message = "";
        String units = Preferences.getWeatherUnits(getApplicationContext());
        String city = "", country = "";

        if (parameters.containsKey("geo-country")){
            country = parameters.get("geo-country").toString();
        }

        if (parameters.containsKey("geo-city")){
            city = parameters.get("geo-city").toString();
        }
        else if (currentCity != null && !currentCity.isEmpty()) {
            city = '"' + currentCity + '"';

            if (country == null){
                country = "";
            }

            if (country.isEmpty()){
                if (currentCountry != null && !currentCountry.isEmpty()){
                    country = '"' + currentCountry + '"';
                    speak("Assuming you want the weather in " + currentCity + ", " + currentCountry);
                    txtV.append("Assuming you want the weather in " + currentCity + ", " + currentCountry);
                }
            }
            else {
                speak("Assuming you want the weather in " + currentCity + ", " + country);
                txtV.append("Assuming you want the weather in " + currentCity + ", " + country);
            }
        }

        if (city == null || city.isEmpty()) {
            speak("An error occurred, can't find the city");
            txtV.append("An error occurred, can't find the city");
            return;
        }

        message = String.format("%s###%s###%s###%s###%s",
                messageCode, Preferences.getUsername(getApplicationContext()),
                city, country, units);

        Connection connectionThread = new Connection();
        connectionThread.start();

        while (!isTheServerFree){}

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connectionError){
                    Toast.makeText(getApplicationContext(), "Can't connect", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        SendMessage sendMessage = new SendMessage(message);
        sendMessage.start();

        GetMessages getMessages = new GetMessages();
        getMessages.start();
    }

    void getNews(HashMap parameters){
        String country = "";
        String message = "";
        String messageCode = "50";

        if(parameters.containsKey("geo-country")) {
            country = parameters.get("geo-country").toString();
        }
        else if (currentCountry != null && !currentCountry.isEmpty()) {
            country = '"' + currentCountry + '"';
            speak("Assuming you want the news in " + currentCountry);
            txtV.append("Assuming you want the news in " + currentCountry);
        }
        else{
            speak("An error occurred. Where are you?");
            txtV.append("An error occurred. Where are you?");
            return;
        }

        message = String.format("%s###%s###%s", messageCode,
                Preferences.getUsername(getApplicationContext()), country);

        Connection connectionThread = new Connection();
        connectionThread.start();

        while (!isTheServerFree){}

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connectionError){
                    Toast.makeText(getApplicationContext(), "Can't connect", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        SendMessage sendMessage = new SendMessage(message);
        sendMessage.start();

        GetMessages getMessages = new GetMessages();
        getMessages.start();
    }

    void searchGoogle(String command){
        String query = command.substring("search ".length());
        String messageCode = "00";
        String username = Preferences.getUsername(getApplicationContext());
        String message = String.format("%s###%s###%s", messageCode, username, query);

        Connection connectionThread = new Connection();
        connectionThread.start();

        while (!isTheServerFree){}

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connectionError){
                    Toast.makeText(getApplicationContext(), "Can't connect", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        SendMessage sendMessage = new SendMessage(message);
        sendMessage.start();

        GetMessages getMessages = new GetMessages();
        getMessages.start();
    }

    void listen() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listenFnc();
            }
        });
    }

    void listenFnc()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        speechRecognizer.startListening(intent);
    }

    void call(String contact) {
        Intent intent = new Intent(Intent.ACTION_CALL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            speak("Read contacts permission not granted!");
            return;
        } else {
            if (getPhoneNumber(contact, this) == "Unsaved") {
                speak("Contact not found. Try again!");
            } else {
                intent.setData(Uri.parse("tel:" + getPhoneNumber(contact, this)));

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    speak("Permission not granted!");
                    return;
                } else {
                    this.startActivity(intent);

                }
            }
        }

    }

    public void getLocation() {
        client = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != //checking that the permissions to access location are given
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            loc = "Location permission not given.";

        }
        client.getLastLocation().addOnSuccessListener(talkScreen.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null)
                {
                    getCity(location);
                }
                else if(loc == null || loc == " ")
                {
                    loc = "error";
                }
            }
        });
    }

    public void getCity(Location location)
    {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geoCoder.getFromLocation(lat, lng, 10);
            if(addresses.size() > 0) {
                for (Address adr : addresses) {
                    if (adr.getLocality() != null && adr.getLocality().length() > 0) {
                        loc = adr.getLocality();
                        break;
                    }
                }
            }
        } catch (IOException e) {
            // Handle IOException
        } catch (NullPointerException e) {
            // Handle NullPointerException
        }
    }

    public String getPhoneNumber(String name, Context context) {
        String ret = null;
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" like'%" + name +"%'";
        String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor c = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, selection, null, null);
        if (c.moveToFirst()) {
            ret = c.getString(0);
        }
        c.close();
        if(ret==null)
            ret = "Unsaved";
        return ret;
    }

    void turnOnRecourse(String command) {
        if (command.contains("wifi") || command.contains("wi-fi")) {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(true);

        } else if (command.contains("bluetooth")) {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            } else {
                speak("Bluetooth is already enabled!");
                txtV.append("Bluetooth is already enabled!");
            }
        } else if (command.contains("flashlight")) {
            if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) && // Checking that we've go the right conditions to activate the flashlight
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                try {
                    String cameraID = cameraManager.getCameraIdList()[0];
                    cameraManager.setTorchMode(cameraID, true);
                    flashlightStatus = true;

                } catch (CameraAccessException e) {
                    speak("An error occurred!");
                }
            } else {
                speak("permission not granted or flashlight does not exist on the device");
            }
        }
    }

    void turnOffResource(String command) {
        if(command.contains("wi-fi") || command.contains("wifi"))
        {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            if (wifiManager != null) {
                wifiManager.setWifiEnabled(false);
            }
        }
        else if (command.contains("bluetooth")) {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.disable();
            } else {
                speak("Bluetooth is already disabled!");
            }
        } else if (command.contains("flashlight"))
        {
            if(flashlightStatus)
            {
                CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                try
                {
                    String cameraID = cameraManager.getCameraIdList()[0];
                    cameraManager.setTorchMode(cameraID, false);
                    flashlightStatus = false;

                } catch (CameraAccessException e)
                {
                    speak("An error occurred!");
                }
            }
            else
            {
                speak("flashlight is not turned on");
            }
        }
    }

    /**
     * Launch an application.
     * @param command A string with the name of the application.
     */
    void openProcess(String command) {
        String appName = command.substring("open ".length());
        String packName = getPackNameByAppName(appName);
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packName);

        if (launchIntent != null) {
            speak("opening " + command);
            startActivity(launchIntent); // null pointer check in case package name was not found
        }
        else
        {
            speak("App does not exist");
            txtV.append("App does not exist");
        }
    }

    /**
     * Gte the package name of an application.
     * @param name The name of the app.
     * @return The app's package name.
     */
    public String getPackNameByAppName(String name) {
        PackageManager pm = getApplicationContext().getPackageManager();
        List<ApplicationInfo> l = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        name = name.toLowerCase();

        for (ApplicationInfo ai : l) {
            String n = pm.getApplicationLabel(ai).toString().toLowerCase();

            if (n.contains(name) || name.contains(n)){
                return ai.packageName;
            }
        }

        return null;
    }

    /**
     * Configure the back button.
     */
    private void configureBackButton() {
        Button talkBtn = findViewById(R.id.back2);

        talkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                Disconnect disconnectThread = new Disconnect();
                disconnectThread.start();

                startActivity(new Intent(talkScreen.this, MainScreen.class));
                finish();
            }
        });
    }

    private class Connection extends Thread{
        private String serverIP;
        private int serverPort;
        private InetAddress serverAddress;

        Connection(){
            serverIP = Preferences.getServerIP(getApplicationContext());
            serverPort = Preferences.getServerPort(getApplicationContext());

            try{
                serverAddress = InetAddress.getByName(serverIP);
            }
            catch (UnknownHostException e){
                Toast.makeText(getApplicationContext(), "Unknown host", Toast.LENGTH_SHORT).show();
                connectionError = true;
            }
        }

        public void run(){
            //isTheServerFree = false;

            if (!connectionError){
                try{
                    socket = new Socket(serverAddress, serverPort);
                }
                catch (IOException e){
                    connectionError = true;
                }

                if (socket == null){
                    connectionError = true;
                }
                else {
                    try {
                        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    }
                    catch (IOException e){
                        connectionError = true;
                    }
                }
            }

            isTheServerFree = true;
        }
    }

    private class GetMessages extends Thread{
        String line;               // The last line from the server

        GetMessages(){ }

        public void run(){
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            try {
                // Get a message from the server.
                line = input.readLine();
                processText(line);
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't get a message.", Toast.LENGTH_SHORT).show();
            }

            isTheServerFree = true;
        }
    }

    private class SendMessage extends Thread{
        private String message;
        OutputStream outputStream;

        SendMessage(String message){
            this.message = message + '\n';
        }

        public void run() {
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            // Try to send the message.
            try {
                outputStream = socket.getOutputStream();
                outputStream.write(message.getBytes(StandardCharsets.UTF_8));
            }
            catch (Exception e){
                Toast.makeText(getApplicationContext(), "Can't send the message.", Toast.LENGTH_SHORT).show();
            }

            // If the message "90", disconnect.
            try {
                if (message.equals("90\n")) {
                    socket.close();
                }
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't close the socket.", Toast.LENGTH_SHORT).show();
            }

            isTheServerFree = true;
        }
    }

    private class Disconnect extends Thread{

        Disconnect(){
        }

        public void run(){

            if (socket != null && socket.isConnected()) {
                while (!isTheServerFree) {
                }

                isTheServerFree = false;

                try{
                    socket.close();
                }
                catch (IOException e){
                    Toast.makeText(getApplicationContext(), "Can;t disconnect.", Toast.LENGTH_SHORT).show();
                }

                isTheServerFree = true;
            }
        }
    }

    private void processGoogleSearch(String[] results){
        String[] resultParts;
        final StringBuilder fullResult = new StringBuilder();

        // Each result consists of 2 parts, separated by "@@@"
        for (int i = 1; i < results.length; ++i){
            resultParts = results[i].split("@@@");

            // Make the title bold
            fullResult.append(String.format("<b>%s:</b><br/>", resultParts[0]));

            for (int j = 1; j < resultParts.length; ++j){
                fullResult.append(String.format("%s<br/>", resultParts[j]));
            }

            fullResult.append("<br/>"); // Make a new line
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtV.setText(Html.fromHtml(fullResult.toString()));
                Linkify.addLinks(txtV, Linkify.ALL);
            }
        });
    }

    private void processWeather(final String[] results){

        if (results[0].equals("11")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final String[] messageParts = results;
                    txtV.append("The temperature is " + messageParts[1] + " degrees and " + messageParts[3]);
                    speak("The temperature is " + messageParts[1] + "degrees and " + messageParts[3]);
                }
            });
        }
        else if (results[0].equals("12")){
            speak("An error occurred, please try again.");
        }
    }

    private void processNews(final String[] results){
        final StringBuilder stringBuilder = new StringBuilder();
        String[] resultParts;

        if (results[0].equals("51")){
            for (int i = 1; i < results.length; ++i){
                resultParts = results[i].split("@@@");

                stringBuilder.append(String.format("<b>%s</b><br/>", resultParts[2])); // The title
                stringBuilder.append(String.format("By %s, %s (%s)<br/>", resultParts[1], resultParts[0], resultParts[4])); // The author, the source and the time
                stringBuilder.append(String.format("%s<br/>", resultParts[3])); // URL

                stringBuilder.append("<br/>");
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtV.setText(Html.fromHtml(stringBuilder.toString()));
                    Linkify.addLinks(txtV, Linkify.ALL);
                }
            });
        }
        else if (results[0].equals("52")){
            speak("An error occurred, please try again.");
        }
    }

    private void processText(String message){
        String[] messageParts = message.split("###");
        String code = messageParts[0];

        if (code.charAt(0) == '0'){
            processGoogleSearch(messageParts);
        }
        else if (code.charAt(0) == '1'){
            processWeather(messageParts);
        }
        else if (code.charAt(0) == '5'){
            processNews(messageParts);
        }
    }

    /*************************************************************************
     *                        LOCATION FUNCTIONS                             *
     *************************************************************************/

    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            @SuppressLint("RestrictedApi") LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(2000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getAddress() {

        if (!Geocoder.isPresent()) {
            Toast.makeText(getApplicationContext(),
                    "Can't find current address, ",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, GetAddressIntentService.class);
        intent.putExtra("add_receiver", addressResultReceiver);
        intent.putExtra("add_location", currentLocation);
        startService(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    Toast.makeText(this, "Location permission not granted, " +
                                    "restart the app if you want the feature",
                            Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == 0) {
                //Last Location can be null for various reasons
                //for example the api is called first time
                //so retry till location is set
                //since intent service runs on background thread, it doesn't block main thread
                Log.d("Address", "Location null retrying");
                getAddress();
            }

            if (resultCode == 1) {
                Toast.makeText(getApplicationContext(),
                        "Address not found, " ,
                        Toast.LENGTH_SHORT).show();
            }

            location = resultData.getString("address_result");

            extractLocationParts();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }

    protected void extractLocationParts(){
        if (location == null || location.isEmpty()){
            return;
        }

        String[] locationParts = location.split("\n");

        currentCity = locationParts[2].split(": ")[1];
        currentCountry = locationParts[5].split(": ")[1];
    }
}
