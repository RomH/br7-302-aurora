package magshimim.aurora;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Use this class' methods to search files.
 */
class FilesOperation {
    /**
     * This function searches files.
     * @param path The path to the area where the function will search files.
     * @param filter A string that is a part of the names of all the files that this function returns.
     * @return An array (ArrayList) of files.
     */
    private static ArrayList<File> searchFiles(String path, String filter){
        ArrayList<File> list;

        // create a File object
        File file = new File(path);

        // if this is a file
        if (file.isFile()){
            int beginningOfFileName = path.lastIndexOf('/');


            // if this is a relevant file
            if (path.substring(beginningOfFileName).contains(filter)){
                list = new ArrayList<>();
                list.add(file);
                return list;
            }

            return null;
        }

        // THE PATH LEADS TO A FOLDER

        File[] filesInDir = file.listFiles();

        if (filesInDir == null){
            return null;
        }

        // this is a list of files in the folder
        ArrayList<File> files = new ArrayList<>(Arrays.asList(filesInDir));
        ArrayList<File> tmp;

        // contains the relevant files
        ArrayList<File> relevantFiles = new ArrayList<>();

        for (File currFile : files){

            // search in this dir
            tmp = searchFiles(currFile.getPath(), filter);

            if (tmp != null){
                relevantFiles.addAll(tmp);
            }
        }

        return relevantFiles;
    }

    /**
     * This function uses "searchFiles" function to search files and returns a string of files.
     * @param path The path to the area where the function will search files.
     * @param filter A string that is a part of the names of all the files that this function returns.
     * @return A string of paths to files.
     */
    static String search(String path, String filter){
        if (filter.isEmpty()){
            return "Error: no filter.";
        }

        // I used StringBuilder because it has the "append" function.
        // The '+' operator copies the whole string, and "append" does not.
        StringBuilder results = new StringBuilder();

        ArrayList<File> files = searchFiles(path, filter);

        if (files == null || files.isEmpty()){
            return "No results.";
        }

        for (File file : files){
            results.append(file.getPath());
            results.append('\n');
        }

        return results.toString();
    }

    /**
     * Create a new folder.
     * The folder will be visible to the user.
     * @param name The name of the folder.
     * @return True for success, false for an error.
     */
    public static boolean createFolder(String name){
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), name);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create a new file.
     * The file will be visible to the user.
     * @param folder The name of the folder.
     * @param name The name of the file.
     * @return True for success, false for an error.
     */
    public static boolean createFile(String folder, String name){
        File file = new File(Environment.getExternalStorageDirectory(), String.format("%s/%s", folder, name));

        if (!file.exists()) {
            try {
                if(!file.createNewFile()){
                    return false;
                }
            } catch (IOException e) {
                return false;
            }
        }

        return true;
    }

    /**
     * Write a string to a file.
     * @param folder The folder that contains the file.
     * @param name The name of the file.
     * @param content The content.
     * @return True for success, false for an error.
     */
    public static boolean writeToFile(String folder, String name, String content){
        try {
            File file = new File(Environment.getExternalStorageDirectory(), String.format("%s/%s", folder, name));
            FileWriter fileWrite = new FileWriter(file);
            fileWrite.write(content);
            fileWrite.flush();
            fileWrite.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Read a string from a file.
     * @param folder The folder that contains the file.
     * @param name The name of the file.
     * @return The content of the file, or a blank string in case of an error.
     */
    public static String readFile(String folder, String name){
        StringBuilder result = new StringBuilder();
        int c = 0;

        try {
            File file = new File(Environment.getExternalStorageDirectory(), String.format("%s/%s", folder, name));
            FileReader fileReader = new FileReader(file);

            c = fileReader.read();

            // -1 indicates the end of the file.
            while (c != -1){
                result.append((char)c);
                c = fileReader.read();
            }
        }
        catch (Exception e){
            return "";
        }

        return result.toString();
    }
}
