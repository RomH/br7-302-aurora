package magshimim.aurora;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;

/**
 * Use this class to request permissions on an activity.
 * Do not forget to add the permissions in the AndroidManifest.xml file!
 */
class RequestPermissions {
    /**
     * Check if an activity has a permission.
     * @param activity The activity that is being checked.
     * @param permission The permission that is being checked.
     * @return true if the activity has granted the permission.
     */
    public static boolean checkPermission(Activity activity, String permission){
        return (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Request a number of permissions on an activity.
     * @param activity The activity that needs the permissions.
     * @param permissions An ArrayList that contains the permissions.
     */
    static void requestPermissions(Activity activity, ArrayList<String> permissions){
        for(int i = 0; i < permissions.size(); i++){
            // Check if the activity has the permission.
            if (checkPermission(activity, permissions.get(i))) {
                // If the answer is 'yes', remove it from the array.
                permissions.remove(i);

                /* The next string's index is i.
                 * At the beginning of the next iteration, i will be increased by 1.
                 * To prevent a loss of a permission, decrease i by 1. */
                i--;
            }
        }

        if (!permissions.isEmpty()){
            // Make a new array of strings / permissions.
            String[] permissionsToRequest = permissions.toArray(new String[permissions.size()]);

            ActivityCompat.requestPermissions(activity, permissionsToRequest,0);
        }
    }
}
