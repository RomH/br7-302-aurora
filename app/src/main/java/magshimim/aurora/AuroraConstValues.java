package magshimim.aurora;

public class AuroraConstValues {
    public final String FLIP_A_COIN = "15";
    public final String PARAMETERS_DONT_EXIST = "0";
    public final String SING = "13";
    public final String CALL_VALUE = "1";
    public final String LOCATION_VALUE = "3";
    public final String OPEN_VALUE = "5";
    public final String TURN_OFF_VALUE = "7";
    public final String TURN_ON_VALUE = "8";
    public final String WEATHER_VALUE = "10";
    public final String NEWS_VALUE = "11";
    public final String LOG_WEIGHT_VALUE = "91";
    public final String GET_WEIGHT_VALUE = "92";
    public final String LOG_HEIGHT_VALUE = "101";
    public final String GET_HEIGHT_VALUE = "102";
    public final String LOG_NAME_VALUE = "111";
    public final String ACTIVATE_MIC = "1000";
    public final String GET_NAME_VALUE = "112";
    public final String ALARM = "12";
    public final String REMEMBER_VALUE = "121";
    public final String GET_REMEMBER_VALUE = "122";
    public final String GENERATE_RANDOM = "18";
    public final String WHAT_CAN_YOU_DO = "17";
    public final String RESET_REMEMBER = "123";
}
