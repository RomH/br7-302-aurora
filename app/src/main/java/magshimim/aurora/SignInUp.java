package magshimim.aurora;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SignInUp extends AppCompatActivity {

    TextToSpeech textToSpeech = null;

    EditText usernameObj;
    EditText passwordObj;

    CheckBox rememberUser;
    Boolean boolIsChecked = false;

    // Get messages from the server
    BufferedReader input;

    // The socket between the client and the server
    private Socket socket;

    // True when the server is free
    private boolean isTheServerFree = false;

    // True if failed to connect to the server
    private boolean connectionError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_up);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = textToSpeech.setLanguage(Locale.UK);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(SignInUp.this,
                                "The language you used is not supported!",
                                Toast.LENGTH_SHORT);
                    } else {
                        textToSpeech.setPitch(1.0f);
                        textToSpeech.setSpeechRate(1.0f);
                    }
                }
            }
        });

        Preferences.setServerIP("51.105.27.210", getApplicationContext());
        Preferences.setServerPort(49152, getApplicationContext());

        requestPermissions();

        usernameObj = findViewById(R.id.username);
        passwordObj = findViewById(R.id.password);
        rememberUser = findViewById(R.id.checkBox);

        usernameObj.setText(Preferences.getUsername(getApplicationContext()));
        passwordObj.setText(Preferences.getPassword(getApplicationContext()));
        rememberUser.setChecked(Preferences.getRememberMe(getApplicationContext()));

        configureLoginButton();
    }

    @Override
    protected void onDestroy(){
        Disconnect disconnectThread = new Disconnect();
        disconnectThread.start();
        super.onDestroy();
    }

    private void configureLoginButton() {
        Button talkBtn = findViewById(R.id.loginBtn);

        talkBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String username = usernameObj.getText().toString();
                String password = passwordObj.getText().toString();
                String message = String.format("%d###%s###%s", 20, username, password);

                if(Preferences.getName(getApplicationContext()) == "" || Preferences.getRememberMe(getApplicationContext()) == false)
                {
                    Preferences.setName(username, getApplicationContext());
                }

                Connection connectionThread = new Connection();
                connectionThread.start();

                while (!isTheServerFree){}

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (connectionError){
                            Toast.makeText(getApplicationContext(), "Can't connect", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                SendMessage sendMessage = null;
                GetMessages getMessages = null;

                if (!username.isEmpty() && !password.isEmpty()){
                    sendMessage = new SendMessage(message);
                    sendMessage.start();

                    getMessages = new GetMessages();
                    getMessages.start();
                }
            }
        });
    }

    private void speak(String txt) {
        textToSpeech.speak(txt, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    /**
     * Request permission from the user.
     */
    private void requestPermissions(){
        // This is an array of permissions to ask.
        ArrayList<String> permissions = new ArrayList<>();

        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.INTERNET);
        permissions.add(Manifest.permission.BLUETOOTH_ADMIN);
        permissions.add(Manifest.permission.CHANGE_WIFI_STATE);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissions.add(Manifest.permission.RECORD_AUDIO);
        permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.READ_CONTACTS);
        permissions.add(Manifest.permission.CALL_PHONE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);

        RequestPermissions.requestPermissions(this, permissions);
    }

    private class Connection extends Thread{
        private String serverIP;
        private int serverPort;
        private InetAddress serverAddress;

        Connection(){
            serverIP = Preferences.getServerIP(getApplicationContext());
            serverPort = Preferences.getServerPort(getApplicationContext());

            try{
                serverAddress = InetAddress.getByName(serverIP);
            }
            catch (UnknownHostException e){
                Toast.makeText(getApplicationContext(), "Unknown host", Toast.LENGTH_SHORT).show();
                connectionError = true;
            }
        }

        public void run(){
            if (!connectionError){
                try{
                    socket = new Socket(serverAddress, serverPort);
                }
                catch (IOException e){
                    connectionError = true;
                }

                if (socket == null){
                    connectionError = true;
                }
                else {
                    try {
                        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    }
                    catch (IOException e){
                        connectionError = true;
                    }
                }
            }

            isTheServerFree = true;
        }
    }

    private class GetMessages extends Thread{
        String line;               // The last line from the server

        GetMessages(){ }

        public void run(){
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            try {
                // Get a message from the server.
                line = input.readLine();
                processAnswer(line);
            }
            catch (IOException e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Can't get a message.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            isTheServerFree = true;
        }
    }

    private class SendMessage extends Thread{
        private String message;
        OutputStream outputStream;

        SendMessage(String message){
            this.message = message + '\n';
        }

        public void run() {
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            // Try to send the message.
            try {
                outputStream = socket.getOutputStream();
                outputStream.write(message.getBytes(StandardCharsets.UTF_8));
            }
            catch (Exception e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Can't send the message.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            // If the message "90", disconnect.
            try {
                if (message.equals("90\n")) {
                    socket.close();
                }
            }
            catch (IOException e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Can't close the socket.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            isTheServerFree = true;
        }
    }

    private class Disconnect extends Thread{

        Disconnect(){
        }

        public void run(){

            if (socket != null && socket.isConnected()) {
                while (!isTheServerFree) {
                }

                isTheServerFree = false;

                try{
                    socket.close();
                }
                catch (IOException e){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Can't disconnect.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                isTheServerFree = true;
            }
        }
    }

    private void processAnswer(String answer){
        // The user is logged in / registered / both
        if (answer.equals("21")){
            if(rememberUser.isChecked())
            {
                if (rememberUser.isChecked()){
                    Preferences.setPassword(passwordObj.getText().toString(),
                            getApplicationContext());
                    Preferences.setRememberMe(true, getApplicationContext());
                }
            }
            else
            {
                // Clear the settings
                Preferences.resetUsername(getApplicationContext());
                Preferences.resetPassword(getApplicationContext());
            }

            Preferences.setUsername(usernameObj.getText().toString(),
                    getApplicationContext());

            // Continue to the main screen

            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

            if (vibrator != null) {
                vibrator.vibrate(100);
            }

            speak("Welcome " + Preferences.getName(getApplicationContext()));

            startActivity(new Intent(SignInUp.this, MainScreen.class));
            finish();
        }
        // The user cannot log-in/register, wrong/taken username/password
        else if (answer.equals("22")){
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

            if (vibrator != null) {
                vibrator.vibrate(200);
            }
        }
    }
}
