package magshimim.aurora;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    static String getRemember(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("Remember", "");
    }

    static String getName(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("user_name", "");
    }

    static String getHeight(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("user_height", "");
    }

    static String getWeight(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("user_weight", "");
    }

    static String getServerIP(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("server_ip", "127.0.0.1");
    }

    static String getWeatherUnits(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("weather_units", "default");
    }

    static String getUsername(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("username", "");
    }

    static String getPassword(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getString("password", "");
    }

    static Integer getServerPort(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).
                getInt("server_port", 49151);
    }

    static boolean getFirstRun(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("is_first_run", true);
    }

    static boolean getRememberMe(Context context){
        return context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).getBoolean("remember_me", false);
    }

    static void setRemember(String Remember, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("Remember", Remember).apply();
    }

    static void setName(String name, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("user_name", name).apply();
    }

    static void setHeight(String height, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("user_height", height).apply();
    }

    static void setWeight(String weight, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("user_weight", weight).apply();
    }

    static void setServerIP(String newIP, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("server_ip", newIP).apply();
    }

    static void setServerPort(Integer newPort, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putInt("server_port", newPort).apply();
    }

    static void setWeatherUnits(String units, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("weather_units", units).apply();
    }

    static void setUsername(String username, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("username", username).apply();
    }

    static void setPassword(String password, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("password", password).apply();
    }

    static void setNotFirstRun(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("is_first_run", false).apply();
    }

    static void setRememberMe(boolean rememberMe, Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("remember_me", rememberMe).apply();
    }

    static void resetServerIP(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("server_ip", "127.0.0.1").apply();
    }

    static void resetServerPort(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putInt("server_port", 49151).apply();
    }

    static void resetWeatherUnits(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("weather_units", "default").apply();
    }

    static void resetUsername(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("username", "").apply();
    }

    static void resetPassword(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putString("password", "").apply();
    }

    static void resetFirstRun(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("is_first_run", true).apply();
    }

    static void resetRememberMe(Context context){
        context.getSharedPreferences("PREFERENCE", context.MODE_PRIVATE).edit()
                .putBoolean("remember_me", false).apply();
    }
}
