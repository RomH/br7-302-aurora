package magshimim.aurora;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

public class typeScreen extends AppCompatActivity {

    private Socket socket;
    private  TextView text;
    private boolean flag = false;
    TextToSpeech textToSpeech;

    // True when the server is free
    private boolean isTheServerFree = false;

    // True if failed to connect to the server
    private boolean connectionError = false;

    BufferedReader input = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Enable fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.type_screen);

        // Set action when clicking on the search button
        Button searchBtn = findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchOnClick();
            }
        });

        // Exit when pressing back
        Button back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        text = findViewById(R.id.results);
        text.setMovementMethod(new ScrollingMovementMethod());

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

                if(status == TextToSpeech.SUCCESS)
                {
                    int result = textToSpeech.setLanguage(Locale.UK);
                    if(result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED)
                    {
                        Toast.makeText(typeScreen.this, "The language you used is not supported!", Toast.LENGTH_SHORT);
                    } else {
                        textToSpeech.setPitch(1.0f);
                        textToSpeech.setSpeechRate(1.0f);
                    }
                }
            }
        });

        //getPreferencesData();

        // Connect to the server.

        Connection connectionThread = new Connection();
        connectionThread.start();

        while (!isTheServerFree){}

        if (connectionError){
            Toast.makeText(getApplicationContext(), "Can't connect", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        else {
            Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(50);
        startActivity(new Intent(typeScreen.this, MainScreen.class));
        finish();
    }

    @Override
    protected void onDestroy()
    {
        if(textToSpeech != null)
        {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }

        Disconnect disconnectThread = new Disconnect();
        disconnectThread.start();

        super.onDestroy();
    }

    private void speak(String txt)
    {
        textToSpeech.speak(txt, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    private void searchOnClick() {
        Switch isInternet = findViewById(R.id.isInternet);
        TextView results = findViewById(R.id.results);
        TextView search = findViewById(R.id.search);
        String request = search.getText().toString();

        Toast.makeText(getApplicationContext(), "Searching...", Toast.LENGTH_SHORT).show();

        if (isInternet.isChecked()) {
            // Search on internet

            if (request.toLowerCase().contains("weather in ")){
                typeScreen.SendMessage sendMessage = new typeScreen.SendMessage("01###"
                + request.substring("weather in ".length(), request.indexOf(',')) // The city
                + "###"
                + request.substring(request.indexOf(',') + 1) // The country
                + "###metric");
                sendMessage.start();

                GetMessages getMessages = new GetMessages();
                getMessages.start();
            }
            else{
                speak("searching for " + search.getText().toString());

                // Send the message
                SendMessage sendMessage = new SendMessage("00###" + search.getText().toString());
                sendMessage.start();

                // Get an answer
                typeScreen.GetMessages getMessages = new typeScreen.GetMessages();
                getMessages.start();
            }
        } else {
            // Not an internet search

            // Search an app
            if (request.contains("open ") || request.contains("Open ")){
                String appName = request.substring("Open ".length());
                String packName = getPackNameByAppName(appName);
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packName);

                speak("launching " + search.getText().toString().toLowerCase().substring("open ".length()));

                if (launchIntent != null) {
                    startActivity(launchIntent); // Null pointer check in case package name was not found
                }
            }
            // Search files
            else {
                speak("searching for " + search.getText().toString());
                results.setText(FilesOperation.search("/storage/emulated/0/", request));
            }
        }
    }

    private class Connection extends Thread{
        private String serverIP;
        private int serverPort;
        private InetAddress serverAddress;

        Connection(){
            serverIP = Preferences.getServerIP(getApplicationContext());
            serverPort = Preferences.getServerPort(getApplicationContext());

            try{
                serverAddress = InetAddress.getByName(serverIP);
            }
            catch (UnknownHostException e){
                Toast.makeText(getApplicationContext(), "Unknown host", Toast.LENGTH_SHORT).show();
                connectionError = true;
            }
        }

        public void run(){
            //isTheServerFree = false;

            if (!connectionError){
                try{
                    socket = new Socket(serverAddress, serverPort);
                }
                catch (IOException e){
                    connectionError = true;
                }

                if (socket == null){
                    connectionError = true;
                }
                else {
                    try {
                        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    }
                    catch (IOException e){
                        connectionError = true;
                    }
                }
            }

            isTheServerFree = true;
        }
    }

    private class GetMessages extends Thread{
        String line;               // The last line from the server

        GetMessages(){ }

        public void run(){
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            try {
                // Get a message from the server.
                line = input.readLine();
                processText(line);
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't get a message.", Toast.LENGTH_SHORT).show();
            }

            isTheServerFree = true;
        }
    }

    private class SendMessage extends Thread{
        private String message;
        OutputStream outputStream;

        SendMessage(String message){
            this.message = message + '\n';
        }

        public void run() {
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            // Try to send the message.
            try {
                outputStream = socket.getOutputStream();
                outputStream.write(message.getBytes(StandardCharsets.UTF_8));
            }
            catch (Exception e){
                Toast.makeText(getApplicationContext(), "Can't send the message.", Toast.LENGTH_SHORT).show();
            }

            // If the message "90", disconnect.
            try {
                if (message.equals("90\n")) {
                    socket.close();
                }
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't close the socket.", Toast.LENGTH_SHORT).show();
            }

            isTheServerFree = true;
        }
    }

    private void processText(String message){
        String[] messageParts = message.split("###");
        String code = messageParts[0];
        StringBuilder result = new StringBuilder();

        // 02 is an answer that contains results from Google
        if (code.equals("02")){
            // Each result consists of 2 parts, separated by "@@@"
            for (int i = 1; i < messageParts.length; ++i){
                String[] tmp = messageParts[i].split("@@@");
                result.append("<b>");       // Make the title bold
                result.append(tmp[0]);      // Add the title
                result.append("<b>:<br/>");  // The rest is not bold, make a new line

                for (int j = 1; j < tmp.length; ++j){
                    result.append(tmp[j]);  // Add the result
                    result.append("<br/>"); // Make a new line
                }

                result.append("<br/>"); // Make a new line
            }

            text.setText(Html.fromHtml(result.toString()));

            Linkify.addLinks(text, Linkify.ALL);
        }
        else if (code.equals("03")){
            for (int i = 1; i < messageParts.length; ++i){
                result.append(messageParts[i]);
                result.append("<br/>");
            }

            text.setText(Html.fromHtml(result.toString()));
        }
    }

    public String getPackNameByAppName(String name) {
        PackageManager pm = getApplicationContext().getPackageManager();
        List<ApplicationInfo> l = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        String packName = "";

        name = name.toLowerCase();

        for (ApplicationInfo ai : l) {
            String n = pm.getApplicationLabel(ai).toString().toLowerCase();

            if (n.contains(name) || name.contains(n)){
                return ai.packageName;
            }
        }

        return null;
    }

    private class Disconnect extends Thread{

        Disconnect(){
        }

        public void run(){

            if (socket != null && socket.isConnected()) {
                try{
                    socket.close();
                }
                catch (IOException e){
                    Toast.makeText(getApplicationContext(), "Can't disconnect.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
