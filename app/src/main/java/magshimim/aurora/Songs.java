package magshimim.aurora;

import java.util.HashMap;
import java.util.Random;

public class Songs {

    HashMap<String, String> songs = new HashMap<String, String>();

    Songs()
    {
        songs.put("7 Rings by Ariana Grande", sevenRings);
        songs.put("Bad Guy by Billie Eilish", badGuy);
        songs.put("Can't Have You by Shawn Mendes", cantHaveYou);
        songs.put("ME! by Taylor Swift", me);
        songs.put("You Need to Calm Down by Taylor Swift", youNeedToCalmDown);
        songs.put("I Don't Care by Ed Sheeran", IDontCare);
        songs.put("No New Friends by LSD", noNewFriends);
        songs.put("Never Go Back by Dennis Lloyd", neverGoBack);
        songs.put("Nana Banana by Netta", nanaBanana);
    }

    public HashMap<String, String> pickRandom()
    {
        HashMap<String, String> retValue = new HashMap<>();
        Object[] keys = songs.keySet().toArray();
        String key = (String) keys[new Random().nextInt(keys.length)];
        String value = songs.get(key);

        retValue.put(key, value);

        return retValue;
    }

    public final String sevenRings = "I want it, I got it, I want it, I got it\n" +
            "I want it, I got it, I want it, I got it\n" +
            "You like my hair? Gee, thanks, just bought it\n" +
            "I see it, I like it, I want it, I got it (Yeah)";

    public final String badGuy = "So you're a tough guy\n" +
            "Like it really rough guy\n" +
            "Just can't get enough guy\n" +
            "Chest always so puffed guy\n" +
            "I'm that bad type\n" +
            "Make your mama sad type\n" +
            "Make your girlfriend mad tight\n" +
            "Might seduce your dad type\n" +
            "I'm the bad guy, duh\n" +
            "I'm the bad guy";

    public final String cantHaveYou = "I can't write one song that's not about you\n" +
            "Can't drink without thinkin' about you\n" +
            "Is it too late to tell you that\n" +
            "Everything means nothing if I can't have you?";

    public final String me = "Me-e-e, ooh-ooh-ooh-ooh\n" +
            "I'm the only one of me\n" +
            "Baby, that's the fun of me\n" +
            "Eeh-eeh-eeh, ooh-ooh-ooh-ooh\n" +
            "You're the only one of you\n" +
            "Baby, that's the fun of you\n" +
            "And I promise that nobody's gonna love you like me-e-e";

    public final String youNeedToCalmDown = "So oh-oh, oh-oh, oh-oh, oh-oh, oh-oh\n" +
            "You need to calm down, you're being too loud\n" +
            "And I'm just like oh-oh, oh-oh, oh-oh, oh-oh, oh-oh (oh)\n" +
            "You need to just stop\n" +
            "Like can you just not step on my gown?\n" +
            "You need to calm down";

    public final String IDontCare = "'Cause I don't care when I'm with my baby, yeah\n" +
            "All the bad things disappear\n" +
            "And you're making me feel like maybe I am somebody\n" +
            "I can deal with the bad nights\n" +
            "When I'm with my baby, yeah\n" +
            "Ooh, ooh, ooh, ooh, ooh, ooh\n" +
            "'Cause I don't care, as long as you just hold me near\n" +
            "You can take me anywhere\n" +
            "And you're making me feel like I'm loved by somebody\n" +
            "I can deal with the bad nights\n" +
            "When I'm with my baby, yeah\n" +
            "Ooh, ooh, ooh, ooh, ooh, ooh";

    public final String noNewFriends = "La-la-la-la, la-la-la-la-la-la-la\n" +
            "No new friends\n" +
            "La-la-la-la, la-la-la-la-la-la-la, ah-la-la-la\n" +
            "No new friends\n" +
            "La-la-la-la, la-la-la-la-la-la-la\n" +
            "No new friends\n" +
            "La-la-la-la, la-la-la-la-la-la-la\n" +
            "No new friends";

    public final String neverGoBack = "I never go back\n" +
            "No, no, no\n" +
            "I never go\n" +
            "I never go back\n" +
            "I never go back\n" +
            "No, no, no\n" +
            "I never go\n" +
            "I never go back";

    public final String nanaBanana = "Nana banana, I do what I wanna (ah)\n"+
            "I do what I wanna do (ooh)\n"+
            "Nana banana, I do what I wanna (ah)\n"+
            "I do what I wanna do (ooh)\n"+
            "Nana banana, I do what I wanna (ah)\n"+
            "I do what I wanna do (ooh)\n"+
            "Nana banana, I do what I wanna (ah)\n"+
            "I do what I wanna do";
}
