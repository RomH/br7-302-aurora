package magshimim.aurora;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        configureUpdateButton();
        configureLogoutButton();
        configureBackButton();

        ((TextView)findViewById(R.id.textView3)).setText("Hello " + Preferences.getName(getApplicationContext()));

        // Show the current settings
        ((TextView)findViewById(R.id.serverIPPlainText)).setText(Preferences.getServerIP(getApplicationContext()));
        ((TextView)findViewById(R.id.serverPortNumber)).setText(Preferences.getServerPort(getApplicationContext()).toString());

        configureUpdateButton();
        configureUnitsSpinner();
    }

    private void configureUpdateButton(){
        Button updateButton = findViewById(R.id.updateSettings);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateServerIP();
                updateServerPort();
            }
        });
    }

    private void configureUnitsSpinner(){
        Spinner spinner = findViewById(R.id.weatherUnitsSpinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.weather_units, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position == 0) {
                    Preferences.resetWeatherUnits(getApplicationContext());
                } else if (position == 1) {
                    Preferences.setWeatherUnits("metric", getApplicationContext());
                } else {
                    Preferences.setWeatherUnits("imperial", getApplicationContext());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        String units = Preferences.getWeatherUnits(getApplicationContext());
        if (units.equals("default")) {
            spinner.setSelection(0);
        }
        else if (units.equals("metric")) {
            spinner.setSelection(1);
        }
        else {
            spinner.setSelection(2);
        }
    }

    private void configureLogoutButton(){
        Button logout = findViewById(R.id.logout);

        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustomTheme));


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setMessage("Are you sure you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("Yes, I would like to log out", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                                vibrator.vibrate(75);

                                Toast.makeText(getApplicationContext(), "Goodbye!", Toast.LENGTH_LONG).show();

                                startActivity(new Intent(SettingsActivity.this, SignInUp.class));
                                finish();
                            }
                        })

                        .setNegativeButton("No, I changed my mind", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(50);
        super.onBackPressed();
    }

    /**
     * Configure the back button.
     */
    private void configureBackButton() {
        Button backBtn = findViewById(R.id.backSettings);

        backBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

                if (vibrator != null) {
                    vibrator.vibrate(50);
                }

                startActivity(new Intent(SettingsActivity.this, MainScreen.class));
            }
        });
    }

    /**
     * Update the server's IP address.
     */
    @SuppressLint("SetTextI18n")
    private void updateServerIP() {
        TextView text = findViewById(R.id.serverIPPlainText);
        TextView notes = findViewById(R.id.serverIPNotes);
        String newIP = text.getText().toString();
        LinearLayout layout = findViewById(R.id.serverIPLayout);

        // If the IP isn't valid, show an error message.
        if (!isValidIP(newIP)){
            notes.setText("This IP address is invalid. " +
                    "A valid IP address contains 4 numbers separated by points. " +
                    "The smallest number can be 0 and the biggest - 255.");
            notes.setVisibility(View.VISIBLE);
            notes.setBackgroundColor(Color.RED);
            layout.setBackgroundColor(Color.RED);
        }
        else {
            notes.setText("Updated!");
            notes.setVisibility(View.VISIBLE);
            notes.setBackgroundColor(Color.GREEN);
            layout.setBackgroundColor(Color.GREEN);
            Preferences.setServerIP(newIP, getApplicationContext());
        }
    }

    /**
     * Update the server's port.
     */
    @SuppressLint("SetTextI18n")
    private void updateServerPort() {
        TextView text = findViewById(R.id.serverPortNumber);
        TextView notes = findViewById(R.id.serverPortNotes);
        LinearLayout layout = findViewById(R.id.serverPortLayout);

        // Get the port from the user.
        int newPort = Integer.parseInt(text.getText().toString());

        // If the port isn't valid, show an error message.
        if (!isValidPort(newPort)){
            notes.setText("This port is invalid. A valid port is a number between 0 and 65536.");
            notes.setVisibility(View.VISIBLE);
            notes.setBackgroundColor(Color.RED);
            layout.setBackgroundColor(Color.RED);
        }
        else {
            notes.setText("Updated!");
            notes.setVisibility(View.VISIBLE);
            notes.setBackgroundColor(Color.GREEN);
            layout.setBackgroundColor(Color.GREEN);
            Preferences.setServerPort(newPort, getApplicationContext());
        }
    }

    /**
     * Check if a string represents a valid IP address.
     * @param ip The string.
     * @return True if the string is a valid IP address, false otherwise.
     */
    private boolean isValidIP(String ip){
        // An IPv4 address contains three points between four numbers.
        // The array will store the numbers.
        String[] numbers = ip.split("\\.");

        if (numbers.length != 4){
            return false;
        }

        // Check each number's validity.
        for (String number : numbers){
            if (Integer.parseInt(number) > 255 || Integer.parseInt(number) < 0){
                return false;
            }
        }

        return true;
    }

    /**
     * Check if a number is a valid port number.
     * @param port The number to check.
     * @return True if the number is valid.
     */
    private boolean isValidPort(int port){
        return port < 65536;
    }
}

