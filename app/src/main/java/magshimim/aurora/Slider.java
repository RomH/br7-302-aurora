package magshimim.aurora;

import android.content.Context;
import android.graphics.Color;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Slider extends PagerAdapter
{
    private Context context;

    // The images that will be used.
    private int[] imageArr = {R.mipmap.aurora_landscape, R.mipmap.microphone,
            R.mipmap.keyboardintro, R.mipmap.search, R.mipmap.weather};

    // The titles of the screens.
    private String[] titleArr = {"WELCOME TO AURORA!", "TALK", "TYPE", "SEARCH", "ASK"};

    // The descriptions for each screen.
    private String[] descriptionArr = {"", "YOU CAN EITHER TALK...", "OR TYPE",
            "YOU CAN SEARCH ON THE INTERNET OR YOUR PHONE...",
            "OR ASK FOR SIMPLE THINGS SUCH AS THE WEATHER"};

    // The background color of each screen.
    private int[] backgroundColorArr = {Color.rgb(55,55,55),
            Color.rgb(239,85,85),
            Color.rgb(110,49,89),
            Color.rgb(1,188,212),
            Color.rgb(46,204,113)};

    /**
     * The constructor.
     * @param context The context.
     */
    public Slider(Context context) {
        this.context = context;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object){
        return view == object;
    }

    /**
     * Get the length of titles array.
     * @return The length of the titles array "titleArr".
     */
    @Override
    public int getCount(){
        return titleArr.length;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((LinearLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide,container,false);
        LinearLayout layoutSlide = view.findViewById(R.id.introLayout);
        ImageView imgSlide = view.findViewById(R.id.slideImg);
        TextView txtTitle= view.findViewById(R.id.txtTitle);
        TextView description = view.findViewById(R.id.txtDescription);

        layoutSlide.setBackgroundColor(backgroundColorArr[position]);
        imgSlide.setImageResource(imageArr[position]);
        txtTitle.setText(titleArr[position]);
        description.setText(descriptionArr[position]);
        container.addView(view);

        return view;
    }
}
