package magshimim.aurora;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class SplashScreen extends AppCompatActivity {
    Animation fromTop;
    ImageView image;
    TextToSpeech textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Link to xml file
        setContentView(R.layout.splashscreen);

        image = findViewById(R.id.imageView2);

        fromTop = AnimationUtils.loadAnimation(this, R.anim.fromtop);

        image.setAnimation(fromTop);

        new CountDownTimer(1600, 1000) {
            /**
             * When the animation is over, start the main activity.
             */
            public void onFinish() {
                startMainActivity();
                finish();
            }

            /**
             * @param millisUntilFinished The amount of time until finished.
             */
            public void onTick(long millisUntilFinished){}
        }.start();
    }

    /**
     * Use this function to make the app speak.
     * @param txt What to say?
     */
    private void speak(String txt) {
        textToSpeech.speak(txt, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    /**
     * Start the main activity.
     */
    public void startMainActivity() {
        Intent intent = new Intent(this, SignInUp.class);
        startActivity(intent);
    }
}
