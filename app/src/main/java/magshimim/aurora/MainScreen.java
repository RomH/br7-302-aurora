package magshimim.aurora;

import android.Manifest;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.view.View;
import android.view.*;

import org.json.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class MainScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Link the activity to the xml file
        setContentView(R.layout.activity_main_screen);

        // If the app is launched for the first time, show the intro screen
        if (Preferences.getFirstRun(getApplicationContext())) {
            // Show start activity
            startActivity(new Intent(MainScreen.this, WelcomeIntro.class));

            // From now on, don't show the intro
            Preferences.setNotFirstRun(getApplicationContext());
        }

        configureTalkActivityButton();
        configureTypeActivityButton();
        configureSettingsActivityButton();
        configureHistoryButton();
    }

    /**
     * Configure the button that opens "Talk Activity".
     */
    private void configureTalkActivityButton() {
        ImageButton talkBtn = findViewById(R.id.talk);

        talkBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainScreen.this, talkScreen.class));
            }
        });
    }

    /**
     * Configure the button that opens "Type Activity".
     */
    private void configureTypeActivityButton() {
        ImageButton button = findViewById(R.id.type);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainScreen.this, typeScreen.class));
            }
        });
    }

    private void configureSettingsActivityButton(){
        ImageButton button = findViewById(R.id.settings);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainScreen.this, SettingsActivity.class);
                intent.putExtra("message", "1");
                startActivity(intent);
            }
        });
    }

    private void configureHistoryButton(){
        ImageButton button = findViewById(R.id.history);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainScreen.this, HistoryActivity.class));
            }
        });
    }
}
