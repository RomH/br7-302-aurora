package magshimim.aurora;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class HistoryActivity extends AppCompatActivity {

    // Get messages from the server
    BufferedReader input;

    // The socket between the client and the server
    private Socket socket;

    // True when the server is free
    private boolean isTheServerFree = false;

    // True if failed to connect to the server
    private boolean connectionError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        // Go fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Connection connectionThread = new Connection();
        connectionThread.start();

        while (!isTheServerFree){}

        if (connectionError){
            Toast.makeText(getApplicationContext(), "Can't connect", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        else {
            Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
        }

        SendMessage sendMessage = new SendMessage("30###" + Preferences.getUsername(getApplicationContext()));
        sendMessage.start();

        GetMessages getMessages = new GetMessages();
        getMessages.start();

        Disconnect disconnect = new Disconnect();
        disconnect.start();
    }

    @Override
    public void onBackPressed() {
        Disconnect disconnectThread = new Disconnect();
        disconnectThread.start();
        startActivity(new Intent(HistoryActivity.this, MainScreen.class));
        finish();
    }

    private class Connection extends Thread{
        private String serverIP;
        private int serverPort;
        private InetAddress serverAddress;

        Connection(){
            serverIP = Preferences.getServerIP(getApplicationContext());
            serverPort = Preferences.getServerPort(getApplicationContext());

            try{
                serverAddress = InetAddress.getByName(serverIP);
            }
            catch (UnknownHostException e){
                Toast.makeText(getApplicationContext(), "Unknown host", Toast.LENGTH_SHORT).show();
                connectionError = true;
            }
        }

        public void run(){
            //isTheServerFree = false;

            if (!connectionError){
                try{
                    socket = new Socket(serverAddress, serverPort);
                }
                catch (IOException e){
                    connectionError = true;
                }

                if (socket == null){
                    connectionError = true;
                }
                else {
                    try {
                        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    }
                    catch (IOException e){
                        connectionError = true;
                    }
                }
            }

            isTheServerFree = true;
        }
    }

    private class GetMessages extends Thread{
        String line;               // The last line from the server

        GetMessages(){ }

        public void run(){
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            try {
                // Get a message from the server.
                line = input.readLine();
                processText(line);
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't get a message.", Toast.LENGTH_SHORT).show();
            }

            isTheServerFree = true;
        }
    }

    private class SendMessage extends Thread{
        private String message;
        OutputStream outputStream;

        SendMessage(String message){
            this.message = message + '\n';
        }

        public void run() {
            // Wait until the server is free
            while (!isTheServerFree){}

            // Start communicating, make the server busy
            isTheServerFree = false;

            // Try to send the message.
            try {
                outputStream = socket.getOutputStream();
                outputStream.write(message.getBytes(StandardCharsets.UTF_8));
            }
            catch (Exception e){
                Toast.makeText(getApplicationContext(), "Can't send the message.", Toast.LENGTH_SHORT).show();
            }

            // If the message "90", disconnect.
            try {
                if (message.equals("90\n")) {
                    socket.close();
                }
            }
            catch (IOException e){
                Toast.makeText(getApplicationContext(), "Can't close the socket.", Toast.LENGTH_SHORT).show();
            }

            isTheServerFree = true;
        }
    }

    private class Disconnect extends Thread{

        Disconnect(){
        }

        public void run(){

            if (socket != null && socket.isConnected()) {
                while (!isTheServerFree) {
                }

                isTheServerFree = false;

                try{
                    socket.close();
                }
                catch (IOException e){
                    Toast.makeText(getApplicationContext(), "Can;t disconnect.", Toast.LENGTH_SHORT).show();
                }

                isTheServerFree = true;
            }
        }
    }

    public void processText(String line){
        String[] messageParts = line.split("###");
        String code = messageParts[0];
        final StringBuilder stringBuilder = new StringBuilder();

        if (code.equals("31")){
            for (int i = 1; i < messageParts.length; ++i){
                stringBuilder.append(i);
                stringBuilder.append('\t');
                stringBuilder.append(messageParts[i]);
                stringBuilder.append('\n');
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView textView = findViewById(R.id.historyTextView);
                    textView.setText(stringBuilder.toString());
                }
            });
        }
    }
}
